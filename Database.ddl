-- Tablespace: task-manager

-- DROP TABLESPACE task-manager;

CREATE TABLESPACE "task-manager"
  OWNER postgres
  LOCATION 'c:\java-learn\DB_Postgres';

ALTER TABLESPACE "task-manager"
  OWNER TO postgres;
  
  

-- Database: task-manager

-- DROP DATABASE "task-manager";

CREATE DATABASE "task-manager"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = "task-manager"
    CONNECTION LIMIT = -1;
	
	


-- Table: public.tm_project

-- DROP TABLE public.tm_project;

CREATE TABLE IF NOT EXISTS public.tm_project
(
    id character varying(50) NOT NULL,
    user_id character varying(50) NOT NULL,
    name character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
    status character varying(50) NOT NULL,
    description character varying(2000) COLLATE pg_catalog."default",
    CONSTRAINT tm_project_pkey PRIMARY KEY (id)
        USING INDEX TABLESPACE "task-manager"
)

TABLESPACE "task-manager";

ALTER TABLE public.tm_project
    OWNER to postgres;
	
-- Table: public.tm_task

-- DROP TABLE public.tm_task;

CREATE TABLE IF NOT EXISTS public.tm_task
(
    id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    created timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
    status character varying(50) COLLATE pg_catalog."default" NOT NULL,
    description character varying(2000) COLLATE pg_catalog."default",
    project_id character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT tm_task_pkey PRIMARY KEY (id)
        USING INDEX TABLESPACE "task-manager",
    CONSTRAINT task_project_fk FOREIGN KEY (project_id)
        REFERENCES public.tm_project (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE "task-manager";

ALTER TABLE public.tm_task
    OWNER to postgres;
	
-- Table: public.tm_user

-- DROP TABLE public.tm_user;

CREATE TABLE IF NOT EXISTS public.tm_user
(
    id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    login character varying(50) COLLATE pg_catalog."default" NOT NULL,
    password character varying(255) COLLATE pg_catalog."default",
    email character varying(50) COLLATE pg_catalog."default",
    first_name character varying(100) COLLATE pg_catalog."default",
    last_name character varying(100) COLLATE pg_catalog."default",
    middle_name character varying(100) COLLATE pg_catalog."default",
    role character varying(50) COLLATE pg_catalog."default" NOT NULL,
    locked_flg boolean NOT NULL DEFAULT false,
    CONSTRAINT tm_user_pkey PRIMARY KEY (id)
        USING INDEX TABLESPACE "task-manager",
    CONSTRAINT tm_user_login_u UNIQUE (login)
        USING INDEX TABLESPACE "task-manager"
)

TABLESPACE "task-manager";

ALTER TABLE public.tm_user
    OWNER to postgres;
	

-- Table: public.tm_session

-- DROP TABLE public.tm_session;

CREATE TABLE IF NOT EXISTS public.tm_session
(
    id character varying(50) NOT NULL,
    user_id character varying(50) NOT NULL,
    created timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
    role character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT tm_session_pkey PRIMARY KEY (id)
        USING INDEX TABLESPACE "task-manager"
)

TABLESPACE "task-manager";

ALTER TABLE public.tm_session
    OWNER to postgres;