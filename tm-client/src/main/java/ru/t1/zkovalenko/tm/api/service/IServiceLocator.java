package ru.t1.zkovalenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

}
