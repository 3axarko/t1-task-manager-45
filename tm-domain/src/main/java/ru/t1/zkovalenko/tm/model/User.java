package ru.t1.zkovalenko.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Session> sessions = new ArrayList<>();

    @NotNull
    @Column(unique = true, nullable = false)
    private String login;

    @NotNull
    @Column(nullable = false, name = "password")
    private String passwordHash;

    @NotNull
    @Column
    private String email = "";

    @NotNull
    @Column(name = "first_name")
    private String firstName = "";

    @NotNull
    @Column(name = "last_name")
    private String lastName = "";

    @NotNull
    @Column(name = "middle_name")
    private String middleName = "";

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(nullable = false, columnDefinition = "boolean default false", name = "locked_flg")
    private Boolean locked = false;

}
