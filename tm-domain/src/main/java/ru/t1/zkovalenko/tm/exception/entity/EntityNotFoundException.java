package ru.t1.zkovalenko.tm.exception.entity;

public final class EntityNotFoundException extends AbstractEntityException {

    public EntityNotFoundException() {
        super("Entity not Found");
    }

}
