package ru.t1.zkovalenko.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.model.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.model.ITaskRepository;

import javax.persistence.EntityManager;

public interface IProjectTaskService {

    @NotNull ITaskRepository getTaskRepository(@NotNull EntityManager entityManager);

    @NotNull IProjectRepository getProjectRepository(@NotNull EntityManager entityManager);

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

    void removeProjectByIndex(@Nullable String userId, @Nullable Integer index);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

}
