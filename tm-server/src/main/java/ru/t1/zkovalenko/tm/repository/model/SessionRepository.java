package ru.t1.zkovalenko.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.repository.model.ISessionRepository;
import ru.t1.zkovalenko.tm.model.Session;

import javax.persistence.EntityManager;

public final class SessionRepository extends AbstractUserOwnerRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return this.getClazz().getSimpleName();
    }

    @Override
    protected @NotNull Class<Session> getClazz() {
        return Session.class;
    }

}
