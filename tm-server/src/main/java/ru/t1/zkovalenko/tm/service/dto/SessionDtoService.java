package ru.t1.zkovalenko.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.repository.dto.IUserOwnerDtoRepository;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.dto.ISessionDtoService;
import ru.t1.zkovalenko.tm.dto.model.SessionDTO;
import ru.t1.zkovalenko.tm.repository.dto.SessionDtoRepository;

import javax.persistence.EntityManager;

public class SessionDtoService extends AbstractUserOwnerDtoService<SessionDTO> implements ISessionDtoService {

    public SessionDtoService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull IUserOwnerDtoRepository<SessionDTO> getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }

}
