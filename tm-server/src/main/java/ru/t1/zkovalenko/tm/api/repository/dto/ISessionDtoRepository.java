package ru.t1.zkovalenko.tm.api.repository.dto;

import ru.t1.zkovalenko.tm.dto.model.SessionDTO;

public interface ISessionDtoRepository extends IUserOwnerDtoRepository<SessionDTO> {

}
