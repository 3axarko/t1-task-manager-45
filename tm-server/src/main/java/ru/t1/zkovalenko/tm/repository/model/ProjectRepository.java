package ru.t1.zkovalenko.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.repository.model.IProjectRepository;
import ru.t1.zkovalenko.tm.model.Project;

import javax.persistence.EntityManager;

public final class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull String getEntityName() {
        return this.getClazz().getSimpleName();
    }

    @NotNull
    @Override
    protected Class<Project> getClazz() {
        return Project.class;
    }

}