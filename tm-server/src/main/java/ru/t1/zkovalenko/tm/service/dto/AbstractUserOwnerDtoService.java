package ru.t1.zkovalenko.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.dto.IUserOwnerDtoRepository;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.dto.IUserOwnerDtoService;
import ru.t1.zkovalenko.tm.dto.model.AbstractUserOwnerModelDTO;
import ru.t1.zkovalenko.tm.enumerated.Sort;
import ru.t1.zkovalenko.tm.exception.entity.EntityNotFoundException;
import ru.t1.zkovalenko.tm.exception.field.IdEmptyException;
import ru.t1.zkovalenko.tm.exception.field.IndexIncorrectException;
import ru.t1.zkovalenko.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnerDtoService<M extends AbstractUserOwnerModelDTO>
        extends AbstractDtoService<M> implements IUserOwnerDtoService<M> {

    public AbstractUserOwnerDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected abstract IUserOwnerDtoRepository<M> getRepository(@NotNull final EntityManager entityManager);

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        return findAll(userId, (Comparator) null);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            return getRepository(entityManager).findAll(userId, comparator);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return findAll(userId, sort == null ? null : sort.getComparator());
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            return getRepository(entityManager).findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            return getRepository(entityManager).findOneByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @Nullable final M model;
        try {
            entityManager.getTransaction().begin();
            model = getRepository(entityManager).removeById(userId, id);
            if (model == null) throw new EntityNotFoundException();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @Nullable M model;
        try {
            entityManager.getTransaction().begin();
            model = getRepository(entityManager).removeByIndex(userId, index);
            if (model == null) throw new EntityNotFoundException();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

}
